const config = require("./config.js");
const fields = require("../battleship/fields.js");

function Draw(bord) {
    this.board = bord;
}

Draw.prototype.draw = function() {
    let firstLine = "".padEnd(3, " ");
    for (let x = 0; x <= config.BATTLEFIELD_WIDTH - 1; x++) {
        firstLine += String.fromCharCode(65 + x).padEnd(2, " ");
    }
    console.log(firstLine);

    for (let i = 0; i <= config.BATTLEFIELD_HEIGHT - 1; i++) {
        let line = String(i + 1).padEnd(3, " ");
        for (let x = 0; x <= config.BATTLEFIELD_WIDTH - 1; x++) {
            let fieldType = this.board.board[i][x];
            if (fieldType === fields.HIT) {
                line += "X".padEnd(2, " ");
            } else if (fieldType === fields.MISS) {
                line += "-".padEnd(2, " ");
            } else if (fieldType === fields.SHIP) {
                line += "*".padEnd(2, " ");
            } else {
                line += " ".padEnd(2, " ");
            }
        }
        console.log(line);
    }
};
module.exports = Draw;
