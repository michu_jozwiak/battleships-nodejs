const Board = require("../battleship/board.js");
const Ship = require("../battleship/ship");
function Game() {}

Game.prototype.buildBoard = function(height, width) {
    this.board = new Board(height, width);
    return this;
};

Game.prototype.setShips = function(shipsArray) {
    this.ships = Array();
    shipsArray.forEach(ship => {
            this.ships.push(new Ship(ship));
        }
    );

    this.board.placeShipsOnBoard(this.ships);
};

Game.prototype.shoot = function(shoot) {
    let shootType = this.board.shoot(shoot[0], shoot[1]);

    console.log(shootType);
};

Game.prototype.isEndGame = function() {
    let sunk = Array();

    this.ships.forEach(function(ship) {
        sunk.push(ship.sunk)
    }, this);

    if (sunk.includes(false)) {
        return false;
    }

    return true;
};
module.exports = Game;
