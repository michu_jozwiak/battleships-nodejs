function Coordinates(coordinates) {}

Coordinates.prototype.translate = function(coordinates) {
    let corArray = coordinates.toUpperCase().trim().match(/[a-z]+|[^a-z]+/gi);
    let letter = corArray[0];
    let number = corArray[1];

    return Array(this.makeY(number), this.makeX(letter));
};

Coordinates.prototype.makeX = function(letter) {
    return letter.charCodeAt(0) - 65;
};

Coordinates.prototype.makeY = function(number) {
    return number - 1;
};
module.exports = Coordinates;
