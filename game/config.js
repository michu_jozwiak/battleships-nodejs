module.exports = {
    BATTLEFIELD_HEIGHT : 10,
    BATTLEFIELD_WIDTH : 10,
    SHIPS : [4, 4, 5],
    HORIZONTAL : 'vertical',
    VERTICAL : 'vertical',
};