const config = require("../game/config.js");
const fields = require("./fields");
const Shot = require("./shot.js");
const _ = require('underscore');


const UP = 'up';
const DOWN = 'down';
const DIRECTION_ARRAY = Array(UP, DOWN);

function Board(height, width) {
    this.height = height;
    this.width = width;
    this.board = Array(this.height).fill(0);
    for (let i = 0; i <= this.width; i++) {
        this.board[i] = Array(this.width).fill(0);
    }
}

Board.prototype.placeShipsOnBoard = function(ships) {
    this.ships = ships;
    this.ships.forEach(function(ship, i) {
        this.randomizePosition(ship);
    }, this);
};

Board.prototype.randomizePosition = function(ship) {
    if(ship.orientation === config.HORIZONTAL) {
        let minStart = Math.ceil(ship.shipSize);
        let maxStart = Math.floor(this.height - ship.shipSize);

        while (true) {
            let startPoint = Math.floor(Math.random() * (maxStart - minStart + 1)) + minStart;
            let column = Math.floor(Math.random() * (this.width - 1 + 1)) + 1;
            let direction = DIRECTION_ARRAY[Math.floor(Math.random() * DIRECTION_ARRAY.length)];

            let positions = Array();
            for (let i = 0; i <= ship.shipSize - 1; i++) {
                if (direction === UP) {
                    positions.push(Array(column, startPoint + i))
                } else {
                    positions.push(Array(column, startPoint - i))
                }
            }

            let canStay = true;
            positions.forEach(position => {
                if (this.board[position[1]][position[0]] !== 0) {
                    canStay = false;
                }
            });

            if (canStay) {
                this.createPosition(ship, positions);
                break;
            }
        }
    }

};

Board.prototype.createPosition = function(ship, shipsPositions) {
    shipsPositions.forEach(function(position) {
        ship.position.push(position);
        this.board[position[0]][position[1]] = 1;
    }, this);

    console.log(ship);
};

Board.prototype.shoot = function (x, y) {
    if (!this.validShoot(x, y)) {
        return Shot.MISS;
    }

    if (this.board[x][y] === fields.SHIP) {
        this.board[x][y] = fields.HIT;

        return this.hitShip(x, y).hitType();
    } else if (this.board[x][y] === fields.HIT) {
        return Shot.MISS;
    } else {
        this.board[x][y] = fields.MISS;

        return Shot.MISS;
    }
};

Board.prototype.validShoot = function(x, y) {
    return x < this.width && y < this.height && x >= 0 && y >= 0;
};

Board.prototype.hitShip = function(x, y) {
    let hitShip = {};
    this.ships.forEach(function(ship) {
        if (ship.hit(x, y)) {
            hitShip = ship;
        }
    });

    return hitShip;
};
module.exports = Board;
