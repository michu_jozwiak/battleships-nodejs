const config = require("../game/config");
const Shot = require("./shot.js");

const ORIENTATION_ARRAY = Array(config.HORIZONTAL, config.VERTICAL);

function Ship(shipSize) {
    this.shipSize = shipSize;
    this.orientation = ORIENTATION_ARRAY[Math.floor(Math.random() * ORIENTATION_ARRAY.length)];
    this.position = Array();
    this.sunk = false;
}

Ship.prototype.hit = function(x, y) {
    if (this.sunk) {
        return false;
    }

    let hit = false;
    this.position.forEach(function(coordinates, i, position) {
        if (JSON.stringify(coordinates) === JSON.stringify(Array(x, y))) {
            position.splice(i, 1);

            hit = true;
        }
    }, this);

    if (this.position.length === 0) {
        this.sunk = true;
    }

    return hit;
};

Ship.prototype.hitType = function() {
    if (this.sunk) {
        return Shot.SUNK;
    }

    return Shot.HIT;
};
module.exports = Ship;
