const readline = require("readline");
const config = require("./game/config");
const Game = require("./game/game.js");
const Draw = require("./game/draw.js");
const Coordinates = require("./game/coordinates.js");

const game = new Game();
game.buildBoard(config.BATTLEFIELD_HEIGHT, config.BATTLEFIELD_WIDTH).setShips(config.SHIPS);
const draw = new Draw(game.board);
const coordinates = new Coordinates();

process.stdout.write("\033c");

const rl = readline.createInterface({input: process.stdin, output: process.stdout});
let question = function(q) {
    return new Promise((res, rej) => {
        rl.question(q, answer => {
            res(answer);
        });
    });
};

(async function main(){
    let userCommand;
    process.stdout.write("\033c");

    draw.draw();

    while (true) {
        console.log("\n" + "1: exit" + "\n");
        userCommand = await question("Pass coordinates (e.g.: b5, c2 ...) or command: ");
        userCommand = String(userCommand).trim();
        if (userCommand === "1") {
            console.log("END OF GAME");
            break;
        }
        process.stdout.write("\033c");
        try {
            game.shoot(coordinates.translate(userCommand));
        } catch (error) {
            console.log(answer, error.message);
        }

        draw.draw();

        if (game.isEndGame()) {
            console.log("YOU WIN!");
            break;
        }
    }

    process.exit(0);
})();
